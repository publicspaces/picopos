# PicoPOS, Simple Mollie QR POS



## Configuration

Create an `.env` file and put the following variables in it:

```
SITE_URL=http://localhost:3210
MOLLIE_API_KEY={your Mollie API key, test or production}
PORT=3210
```

Define your products in `public/products.json`:

```json
{"products":[
 {"name":"T-shirt", "price": 21.00, "tax":21,"code": "TSHIRT", "color":"#FAA2FF"},
 {"name":"Ticket", "price": 50.00, "tax":21,"code": "TICKET", "color":"#FFA2B5"},
 {"name":"Book 1", "price": 25.00, "tax":9,"code": "BOOK1", "color":"#FFFEA2"},
 {"name":"Other", "price": 20.00, "tax":0,"code": "other", "color":"#A2B7FF"}
]}
```

Optionaly you can define two webhooks using environment variables. 

The first one will be called when a payment-link is created and will have the products in the shoppingcart as the payload:

```
WEBHOOK_URL=https://somebackend.com/picopos
```

Example payload:

```json
{
  "payment_link_id": "#mollie_id_of_the_payment_link#",
  "description": "3 x T-Shirt, 1 x Ticket",
  "cart":[
     {"name":"T-shirt", "price": 21.00, "tax":21,"code": "TSHIRT", "color":"#FAA2FF"},
     {"name":"T-shirt", "price": 21.00, "tax":21,"code": "TSHIRT", "color":"#FAA2FF"},
     {"name":"T-shirt", "price": 21.00, "tax":21,"code": "TSHIRT", "color":"#FAA2FF"},
     {"name":"Ticket", "price": 50.00, "tax":21,"code": "TICKET", "color":"#FFA2B5"}
  ]
}
```

The second webhook is a URL that's called by Mollie when a payment is made. The webhook can be used to check the status of the payment. See https://docs.mollie.com/overview/webhooks for more info.

```
MOLLIE_WEBHOOK_URL=https://somebackend.com/mollie
```

## Development

Setup your development enviornment:

```
npm install
```

and run:

```
npm run dev
```

## Production

Build the production code:

```
npm run build
```

Start the production server:

```
npm start
```

## Docker

build a Docker image:

```
docker build -t picopos .
```

run in docker container:

```
docker run -p 127.0.0.1:3210:3210/tcp --env-file .env picopos:latest
```


## Docker Compose

Example `docker-compose.yml` using traefik-reverse proxy

```
services:
  kassa:
    image: registry.gitlab.com/publicspaces/picopos:main
    env_file: .env
    networks:
      - web
    volumes:
      - ./products.json:/app/dist/products.json
    restart: always
    labels:
      - "traefik.docker.network=web"
      - "traefik.enable=true"
      - "traefik.http.routers.pubconf.rule=Host(`picopos.example.com`)"
      - "traefik.http.routers.pubconf.entrypoints=websecure"
      - "traefik.http.routers.pubconf.tls=true"
      - "traefik.http.routers.pubconf.tls.certresolver=lets-encrypt"
      - "traefik.port=3210"


networks:
  web:
    external: true
```