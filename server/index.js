import {config} from 'dotenv';
import express from "express";
import path from "path";

config();

import homepageRouter from "./homepageRouter.js";
import assetsRouter from "./assetsRouter.js";

const port = process.env.PORT || 3210;
const siteUrl = process.env.SITE_URL || `http://localhost:${port}`;

const publicPath = path.join(path.resolve(), "public");
const distPath = path.join(path.resolve(), "dist");

const app = express();


import fetch  from 'node-fetch';

// Mollie API configuration
const API_KEY = process.env.MOLLIE_API_KEY;
const API_BASE_URL = 'https://api.mollie.com/v2/';

// Generate a payment link
async function generatePaymentLink(amount, description, products, name, email, redirectUrl) {
  try {

    let body = {
        amount: {
          currency: 'EUR',
          value: amount.toFixed(2),
        },
        description,
        redirectUrl,
      };

    // add webhoookUrl if present
    if (process.env.MOLLIE_WEBHOOK_URL) body.webhookUrl = process.env.MOLLIE_WEBHOOK_URL;

    const response = await fetch(`${API_BASE_URL}payment-links`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${API_KEY}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    });

    const data = await response.json();

    if (process.env.WEBHOOK_URL) {
      try {
        const webhook = process.env.WEBHOOK_URL
        const response = await fetch(webhook, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            payment_link_id: data.id,
            amount: data.amount,
            description: data.description,
            cart: products,
            email: email,
            name: name
          }),
        });

      } catch(error) {
        console.log(`Error calling WEBHOOK_URL (${webhook})`)
        console.log(error);
      }
    }
    const paymentLink = data._links.paymentLink.href;
    
    return paymentLink;
  } catch (error) {
    console.error('Error generating payment link:', error);
    throw error;
  }
}






if (process.env.NODE_ENV === "production") {
  app.use("/", express.static(distPath));
} else {
  app.use("/", express.static(publicPath));
  app.use("/src", assetsRouter);
}
app.use(homepageRouter);
app.use(express.json());

app.post("/api/v1/pay", async  (_req, res) => {

  const {products, name, email}= _req.body;

  let amount = 0;
  let counts = {};

  for (let p of products) {
    amount += p.price
    if (counts[p.name]) counts[p.name]++;
    else counts[p.name] = 1;
  }

  let description = [];
  for (let i of Object.keys(counts)) {
    description.push (`${counts[i]} x ${i}`);
  }
  description = description.join(", ")

  // Example usage
  const paymentLink = await generatePaymentLink(amount, description, products, name, email, `${siteUrl}/thanks`);

  res.json({ status: 200, url: paymentLink });




});

app.listen(port, () => {
  console.log("Server listening on port", port);
});
